## Debian Tcl/Tk policy

This is the HTML version of the Debian Tcl/Tk policy. It's accessible
via the following URL:

<https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/>

The Policy sources can be obtained from the
[debian-tcltk-policy](https://salsa.debian.org/tcltk-team/debian-tcltk-policy)
project.
